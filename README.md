# Pokédex

Este projeto foi desenvolvido na IDE Android Studio.

O link para download do apk está disponível em: https://drive.google.com/open?id=13_vLp85I-PE4-C0Mo64IJe05pSaDVCGv

## Uso

A tela inicial do aplicativo dispõe de dois botões, Treinador e Pokemon.

### Pokemon
Quando o botão Pokemon é selecionado na tela inicial, inicia-se a tela de visualização dos pokémon.
Onde é possível visualizar o nome e imagem de todos os Pokémons e realizar pesquisas em tempo real para encontrar o pokémon desejado.
Ao clicar em um pokémon é aberta uma tela com seus stats.

### Treinador

Após selecionar o botão treinador, inicia-se a tela de opções para treinador. O intuiro é visualizar os treinadores ou criar um.

