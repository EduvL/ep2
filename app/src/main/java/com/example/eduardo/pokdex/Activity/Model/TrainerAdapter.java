package com.example.eduardo.pokdex.Activity.Model;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.eduardo.pokdex.Activity.Activity.Trainerinfo;
import com.example.eduardo.pokdex.R;

import java.util.ArrayList;

public class TrainerAdapter extends RecyclerView.Adapter<TrainerAdapter.ViewHolder> {
    private ArrayList<Treinador> dataset;
    private Context context;

    public TrainerAdapter(){
        dataset = new ArrayList<>();
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.traineritem, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TrainerAdapter.ViewHolder holder, int position) {
        final Treinador trainer = dataset.get(position);
        holder.trainername.setText(trainer.getNome());
        final String name = trainer.getNome();
        holder.trainername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Trainerinfo.class);
                intent.putExtra("Nome",name);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {

        return dataset.size();

    }

    public void addtrainerlist(ArrayList<Treinador> trainerlist) {
        dataset.addAll(trainerlist);
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView trainername;

        public ViewHolder(View itemView) {
            super(itemView);
            trainername = itemView.findViewById(R.id.trainername);
        }
    }
}
