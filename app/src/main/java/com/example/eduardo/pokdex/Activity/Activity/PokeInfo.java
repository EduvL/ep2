package com.example.eduardo.pokdex.Activity.Activity;

import android.os.Build;
import android.os.StrictMode;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.TextView;

import com.example.eduardo.pokdex.Activity.ApiService.Connection;
import com.example.eduardo.pokdex.R;

import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class PokeInfo extends AppCompatActivity {
    String name;
    int number;
    String pspeed;
    String pdefense;
    String pattack;
    String pspdefense;
    String pspattack;
    private static final String TAG = "STATS";
    TextView speed;
    TextView pokename;
    TextView attack;
    TextView defense;
    TextView spattack;
    TextView spdefense;
    JSONObject obj;
    JSONParser parser;
    String pokestring;
    BufferedReader reader;
    int code;
    StringBuffer pokebuffer;
    Connection request;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poke_info);
        speed = findViewById(R.id.speed);
        pokename = findViewById(R.id.pokename);
        attack = findViewById(R.id.attack);
        defense = findViewById(R.id.defense);
        spattack = findViewById(R.id.spattack);
        spdefense = findViewById(R.id.spdefense);
        name = getIntent().getStringExtra("Name");
        number = getIntent().getIntExtra("Number", 1);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        getWindow().setLayout((int) (width * .8), (int) (height * .6));
        try {
            statsconnection(number);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void statsconnection(int number) throws MalformedURLException, IOException, ParseException, JSONException {
        request = new Connection();
        String response = request.linkStart("https://pokeapi.co/api/v2/pokemon/"+number+"/");
        parser = new JSONParser();
        Log.d(TAG, response);
        JSONObject obj = new JSONObject();
        obj = (JSONObject) parser.parse(response);

        JSONArray array = (JSONArray) obj.get("stats");
        pokename.setText(name);
        /*for(int i = 0; i<5; i++) {
        obj = (JSONObject) array.get(i);
           if(i==0) {
                pspeed = obj.get("base_stats").toString();
                speed.setText(pspeed);
            }
            if(i==1){
                pdefense = obj.get("base_stats").toString();
                spdefense.setText(pdefense);
            }
            if(i==2){
                pspattack = obj.get("base_stats").toString();
                spattack.setText(pspattack);
            }
            if(i==3){
                pspdefense = obj.get("base_stats").toString();
                defense.setText(pspdefense);
            }
            if(i==4){
                pattack = obj.get("base_stats").toString();
                attack.setText(pattack);
            }
        }*/
    }
}


