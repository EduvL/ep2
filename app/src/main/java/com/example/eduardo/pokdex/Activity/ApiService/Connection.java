package com.example.eduardo.pokdex.Activity.ApiService;

import android.os.Build;
import android.os.StrictMode;
import android.support.annotation.RequiresApi;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

import static android.support.constraint.Constraints.TAG;

public class Connection {
    public Connection() {
        this.url = "";
    }

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public String linkStart(String url) throws IOException {
        this.url = url;
        String TAG = "CONN";
        StringBuilder pokebuffer = new StringBuilder();
        String pokestring;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        URL path = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) path.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");

        int code = connection.getResponseCode();
        if (code == HttpURLConnection.HTTP_OK) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            while (!Objects.equals(pokestring = reader.readLine(), null)) {
                Log.d(TAG, String.valueOf(pokestring));
                pokebuffer.append(pokestring);
            }
            reader.close();
        }else {
            Log.e(TAG, "CONNECTION ERROR");
        }
        connection.disconnect();
        Log.d(TAG, pokebuffer.toString());
        return pokebuffer.toString();
    }
}
