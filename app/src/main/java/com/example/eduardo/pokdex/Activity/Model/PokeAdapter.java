package com.example.eduardo.pokdex.Activity.Model;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.eduardo.pokdex.Activity.Activity.BeginActivity;
import com.example.eduardo.pokdex.Activity.Activity.MainActivity;
import com.example.eduardo.pokdex.Activity.Activity.PokeInfo;
import com.example.eduardo.pokdex.R;
import android.content.Intent;
import java.util.ArrayList;



public class PokeAdapter extends RecyclerView.Adapter<PokeAdapter.ViewHolder> {
    private ArrayList<Pokemon> dataset;
    private Context context;
    private ArrayList<PokeSearch> datasearch;
    

    public PokeAdapter(Context context) {
        this.context = context;
        dataset = new ArrayList<>();
        datasearch = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pokemon, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            final Pokemon p = dataset.get(position);
            holder.poketext.setText(p.getName());
                 Glide.with(context)
                         .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"+ p.getNumber() +".png")
                         .into(holder.pokeimagem);
        final int number = p.getNumber();
        final String name = p.getName();

        holder.pokeimagem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PokeInfo.class);
                intent.putExtra("Name", name);
                intent.putExtra("Number", number);
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void addpokelist(ArrayList<Pokemon> pokelist) {
        dataset.addAll(pokelist);
        notifyDataSetChanged();
    }

    public void search(ArrayList<Pokemon> pokesearch) {
        dataset = pokesearch;
        notifyDataSetChanged();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView pokeimagem;
        private TextView poketext;

        public ViewHolder(View itemView) {
            super(itemView);
            pokeimagem = itemView.findViewById(R.id.pokeimagem);
            poketext = itemView.findViewById(R.id.poketext);

        }
    }
}
