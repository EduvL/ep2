package com.example.eduardo.pokdex.Activity.ApiService;

import com.example.eduardo.pokdex.Activity.Model.PokemonResposta;

import retrofit2.Call;
import retrofit2.http.GET;

public interface PokeService {
    @GET("pokemon")
    Call<PokemonResposta> obterListaPokemon();

}
