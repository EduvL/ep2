package com.example.eduardo.pokdex.Activity.Model;

import java.util.ArrayList;

public class Treinador {
    private String nome;

    public Treinador(String nome){
        this.nome = nome;
    }
    public Treinador(String nome, ArrayList<Pokemon> pokemanos){
        this.nome = nome;
        this.pokemanos = pokemanos;
    }



    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ArrayList<Pokemon> getPokemanos() {
        return pokemanos;
    }

    public void setPokemanos(ArrayList<Pokemon> pokemanos) {
        this.pokemanos = pokemanos;
    }

    private ArrayList<Pokemon> pokemanos;
}
