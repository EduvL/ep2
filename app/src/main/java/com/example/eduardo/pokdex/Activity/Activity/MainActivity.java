package com.example.eduardo.pokdex.Activity.Activity;

import android.graphics.Typeface;
import android.nfc.Tag;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.eduardo.pokdex.Activity.ApiService.PokeService;
import com.example.eduardo.pokdex.Activity.ApiService.SearchService;
import com.example.eduardo.pokdex.Activity.Model.PokeAdapter;
import com.example.eduardo.pokdex.Activity.Model.PokeSearch;
import com.example.eduardo.pokdex.Activity.Model.Pokemon;
import com.example.eduardo.pokdex.Activity.Model.PokemonResposta;
import com.example.eduardo.pokdex.Activity.Model.TypeSearch;
import com.example.eduardo.pokdex.R;

import org.json.*;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity {
    Retrofit retrofit, retrosearch;
    private static final String TAG = "POKEDEX";
    private RecyclerView recyclerView;
    private PokeAdapter pokeAdapter;
    private ArrayList<Pokemon> pokelist;
    private ArrayList<PokeSearch> typepokemon;
    Button bug, dragon, elec, fight, fire, fly, ghost, grass, ground, ice, normal, poison, psy, rock, steel, water, dark;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bug = findViewById(R.id.bugbtn);
        dragon = findViewById(R.id.dragonbtn);
        elec = findViewById(R.id.elecbtn);
        fight = findViewById(R.id.fightbtn);
        fire = findViewById(R.id.firebtn);
        fly = findViewById(R.id.flybtn);
        ghost = findViewById(R.id.ghostbtn);
        grass = findViewById(R.id.grassbtn);
        ground = findViewById(R.id.groundbtn);
        ice = findViewById(R.id.icebtn);
        normal = findViewById(R.id.normalbtn);
        poison = findViewById(R.id.poisonbtn);
        psy = findViewById(R.id.psybtn);
        rock = findViewById(R.id.rockbtn);
        steel = findViewById(R.id.steelbtn);
        water = findViewById(R.id.waterbtn);
        dark = findViewById(R.id.darkbtn);
        recyclerView = findViewById(R.id.recyclerview);
        pokeAdapter = new PokeAdapter(this);
        recyclerView.setAdapter(pokeAdapter);
        recyclerView.setHasFixedSize(true);
        final GridLayoutManager layoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(layoutManager);
        final EditText search = findViewById(R.id.searchtext);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });
        retrosearch = new Retrofit.Builder()
                .baseUrl("https://pokeapi.co/api/v2/type/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl("https://pokeapi.co/api/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        obterDados();

        bug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchtype(7);
            }
        });
        dragon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchtype(16);
            }
        });
        elec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchtype(13);
            }
        });
        fight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchtype(2);
            }
        });
        fire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchtype(10);
            }
        });
        fly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchtype(3);
            }
        });
        ghost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchtype(8);
            }
        });
        grass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchtype(12);
            }
        });
        ground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchtype(5);
            }
        });
        ice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchtype(15);
            }
        });
        normal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchtype(1);
            }
        });
        poison.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchtype(4);
            }
        });
        psy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchtype(14);
            }
        });
        rock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchtype(6);
            }
        });
        steel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchtype(9);
            }
        });
        water.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchtype(11);
            }
        });
        dark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchtype(17);
            }
        });
    }

    private void searchtype(int i) {
        SearchService searchService = retrosearch.create(SearchService.class);
        Call<TypeSearch> search = searchService.searchcall(i);
        search.enqueue(new Callback<TypeSearch>() {
            @Override
            public void onResponse(Call<TypeSearch> call, Response<TypeSearch> response) {
                if(response.isSuccessful()){
                    TypeSearch typeSearch = response.body();
                    Log.d(TAG, String.valueOf(response.body()));
                    typepokemon = typeSearch.getPokemon();
                    //Log.d(TAG, String.valueOf(typepokemon));
                    for (int c = 0;c<typepokemon.size();c++ ){
                        pokelist.add(typepokemon.get(c).getPokemon());
                    }
                    //Log.d(TAG, String.valueOf(pokelist));
                    pokeAdapter.search(pokelist);

                }
                else {
                    Log.e(TAG, "onResponse: " + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<TypeSearch> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    private void filter(String s) {
        ArrayList<Pokemon> pokesearch = new ArrayList<>();
        for(Pokemon p : pokelist){
            if(p.getName().toLowerCase().contains(s.toLowerCase())){
                pokesearch.add(p);
            }
        }
        pokeAdapter.search(pokesearch);
    }

    private void obterDados(){
        PokeService service = retrofit.create(PokeService.class);
        Call<PokemonResposta> pokeresposta = service.obterListaPokemon();
        pokeresposta.enqueue(new Callback<PokemonResposta>() {
            @Override
            public void onResponse(Call<PokemonResposta> call, Response<PokemonResposta> response) {
                if(response.isSuccessful()){
                    PokemonResposta pokemonResposta = response.body();
                    pokelist = pokemonResposta.getResults();
                    pokeAdapter.addpokelist(pokelist);

                }
                else {
                    Log.e(TAG, "onResponse: " + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<PokemonResposta> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }
}

