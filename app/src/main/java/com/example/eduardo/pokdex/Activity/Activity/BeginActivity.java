package com.example.eduardo.pokdex.Activity.Activity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.eduardo.pokdex.R;

public class BeginActivity extends AppCompatActivity {
    Button pokebutton;
    Button trainerbutton;
    MediaPlayer poketop;
    ImageView logo;
    int fon = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_begin);
        pokebutton = findViewById(R.id.pokebutton);
        trainerbutton = findViewById(R.id.trainerbutton);
        poketop = MediaPlayer.create(this, R.raw.pokeaudiotop);
        logo = findViewById(R.id.imageView);
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(fon == 5){
                    poketop.start();
                }
                fon++;
            }
        });
        final MediaPlayer pokemedia = MediaPlayer.create(this, R.raw.buttonsound);

        pokebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pokemedia.start();
                startActivity(new Intent(BeginActivity.this, MainActivity.class));

            }
        });
        trainerbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pokemedia.start();
                startActivity(new Intent(BeginActivity.this, TrainerActivity.class));
            }
        });
    }
}
