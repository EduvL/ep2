package com.example.eduardo.pokdex.Activity.Model;

import java.util.ArrayList;

public class TypeSearch {
    private ArrayList<PokeSearch> pokemon;

    public void setPokemon(ArrayList<PokeSearch> pokemon) {
        this.pokemon = pokemon;
    }

    public ArrayList<PokeSearch> getPokemon() {

        return pokemon;
    }

}
