package com.example.eduardo.pokdex.Activity.Model;

public class Pokemon {
    int number;
    private String name;
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getNumber() {
        String[] urlnum = url.split("/");
    return Integer.parseInt(urlnum[urlnum.length-1]);
    }

    public void setNumber(int number) {
        this.number = number;
    }

}
