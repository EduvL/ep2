package com.example.eduardo.pokdex.Activity.Activity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.eduardo.pokdex.R;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class NewTrainer extends AppCompatActivity {
    private static final String TAG = "FILE" ;
    EditText trainername;
    Button add;
    MediaPlayer btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_trainer);
        add = findViewById(R.id.addbutton);
        btn = MediaPlayer.create(this, R.raw.buttonsound);
        add.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                btn.start();
                try {
                    newtrainer();
                } catch (IOException e){
                    try{
                        File dir = new File(getDataDir() + "/files/");
                        File file = new File(getDataDir() + "/files/treinadores.txt");
                        dir.mkdir();
                        file.createNewFile();
                        newtrainer();
                    } catch (IOException f){
                        Log.e(TAG,"Erro ao criar arquivo treinador");
                    }
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void newtrainer() throws IOException {
        File newfile;
        trainername = findViewById(R.id.trainerentry);
        String nome = String.valueOf(trainername.getText());
        Log.d(TAG,nome);
        if (nome == null) {
            Toast.makeText(this, "Insira um nome", Toast.LENGTH_LONG);
        } else {
            BufferedReader buffreader = new BufferedReader(new FileReader(getDataDir() + "/files/treinadores.txt"));

            String readline = buffreader.readLine();
            buffreader.close();

            BufferedWriter buffwriter = new BufferedWriter(new FileWriter(getDataDir() + "/files/treinadores.txt"));

            if (readline == null) {
                buffwriter.append(nome);
            } else {
                buffwriter.append(readline + "/" + nome);
            }
            buffwriter.close();
            newfile = new File(getDataDir() + "/files/" + nome + ".txt");
            newfile.createNewFile();
            BufferedWriter trainerfile = new BufferedWriter(new FileWriter(getDataDir() + "/files/" + nome + ".txt"));
            trainerfile.close();
            buffreader = new BufferedReader(new FileReader(getDataDir() + "/files/treinadores.txt"));
            readline = buffreader.readLine();
            Log.d(TAG,readline);
            startActivity(new Intent(NewTrainer.this, TrainerActivity.class));
        }
    }
}
