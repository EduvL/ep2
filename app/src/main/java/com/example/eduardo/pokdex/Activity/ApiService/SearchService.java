package com.example.eduardo.pokdex.Activity.ApiService;

import com.example.eduardo.pokdex.Activity.Model.TypeSearch;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface SearchService {
    @GET("{number}/")
    Call<TypeSearch> searchcall(@Path("number") int number);
}
