package com.example.eduardo.pokdex.Activity.Activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.eduardo.pokdex.Activity.Model.TrainerAdapter;
import com.example.eduardo.pokdex.Activity.Model.Treinador;
import com.example.eduardo.pokdex.R;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class TrainerActivity extends AppCompatActivity {
    private static final String TAG = "PATH" ;
    private RecyclerView trainerview;
    private TrainerAdapter trainerAdapter;
    Button tbutton;
    MediaPlayer button;
    ArrayList<Treinador> treinadores;

    @TargetApi(Build.VERSION_CODES.N)
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trainer);
        tbutton = findViewById(R.id.newtrainer);
        button = MediaPlayer.create(this, R.raw.buttonsound);
        trainerview = findViewById(R.id.recyclertrainer);
        trainerAdapter = new TrainerAdapter();
        trainerview.setAdapter(trainerAdapter);
        trainerview.setHasFixedSize(true);
        final GridLayoutManager layoutManager = new GridLayoutManager(this, 1);
        trainerview.setLayoutManager(layoutManager);
        treinadores = new ArrayList<>();

        try {
            lerTreinadores();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        tbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button.start();
                startActivity(new Intent(TrainerActivity.this, NewTrainer.class));
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void lerTreinadores() throws IOException {
        BufferedReader buffreader = new BufferedReader(new FileReader(getDataDir() + "/files/treinadores.txt"));
        String line;
        line = buffreader.readLine();
        String[] test = line.split("/");
        //Log.d(TAG, test[1]);
        //String[] names;
        //names = new String[0];
        String read;

        while((read = buffreader.readLine())!=null){
            String names[] = read.split("/");
            Log.d(TAG, names[0]);
            for(int c = 0; c<names.length;c++){
                treinadores.add(new Treinador(names[c]));
            }

        }
        trainerAdapter.addtrainerlist(treinadores);
        Log.d(TAG, String.valueOf(treinadores));



    }


}
